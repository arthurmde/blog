require 'test_helper'

class PostTest < ActiveSupport::TestCase
  test "should not save post without title or body" do
    post = Post.new
    assert_not post.save

    post.title = "My Post"
    assert_not post.save

    post = Post.new
    post.body = "My Post's body"
    assert_not post.save
  end

  test "should properly save post" do
    post = Post.new(:title => "My Post", :body => "My Post's body")
    assert post.save
  end

  test "should have a related comment" do
    post = Post.new(:title => "My Post", :body => "My Post's body")
    comment = Comment.new(:body => "My Comment body")
    post.comments << comment

    assert_not post.comments.empty?
  end
end
