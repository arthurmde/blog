require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "should not save comment without post or body" do
    post = Post.new(:title => "My Post", :body => "My Post's body")
    post.save!

    comment = Comment.new
    assert_not comment.save

    comment.body = "My Comment's body"
    assert_not comment.save

    comment = Comment.new
    post.comments << comment
    assert_not comment.save
  end

  test "should properly save comment" do
    post = Post.create(:title => "My Post", :body => "My Post's body")
    comment = Comment.create(:body => "My Comment body", :post_id => post.id)
    assert comment.save
  end
end
